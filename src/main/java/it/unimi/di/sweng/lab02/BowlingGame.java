package it.unimi.di.sweng.lab02;

public class BowlingGame implements Bowling {
	
	private static final int MAX_ROLLS = 20;
	private int[] rolls = new int[21];
	private int currentRoll = 0;
	
	@Override
	public void roll(int pins) {
		rolls[currentRoll++] = pins;
	}

	@Override
	public int score() {
		int score = 0;
		for(int currentRoll = 0; currentRoll <= MAX_ROLLS; currentRoll++){
			if(rolls[currentRoll] != -1){
				score += rolls[currentRoll];
				if(isSpare(currentRoll))
					score += rolls[currentRoll+1];
				if(isStrike(currentRoll)){
					score += rolls[currentRoll+1] + rolls[currentRoll+2];
					for(int i = MAX_ROLLS; i > currentRoll+1; i--) {                
					    rolls[i] = rolls[i-1];
					}
					rolls[currentRoll+1] = -1;
				}
			}
		}
		return score;
	}
	
	private boolean isSpare(int currentRoll){
		return currentRoll > 0 && currentRoll < MAX_ROLLS && currentRoll % 2 == 1 && rolls[currentRoll] + rolls[currentRoll-1] == 10;
	}
	private boolean isStrike(int currentRoll){
		return currentRoll % 2 == 0 && currentRoll < 18 && rolls[currentRoll] == 10;
	}

}
